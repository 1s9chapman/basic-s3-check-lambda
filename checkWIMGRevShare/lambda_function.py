#   Slack Alert to monitor wimg shared revenue files
#   Expected Run times @ xx:10, 10mins after the hour every 4 hours
#   DST: 00,04,08,12,16,20
#   !DST: 01,05,09,13,17,21
#   

import boto3
from time import strftime,gmtime,localtime
from slack_webhook import Slack

local_time = localtime()
today_date = strftime('%Y-%m-%d', gmtime())
today_time = strftime('%H', gmtime())
# this is the slack webhook to notify the #revenue_updates channel
slack = Slack(url='https://hooks.slack.com/services/webhook/id')
s3 = boto3.client('s3')

# this is the main lambda_handler function
def lambda_handler(event, context):

    check = check_dst();
    if(check == False) :
        return {
            'statusCode': 200,
            'body': 'Not the time to check for files'
        }  
    else :
        bucket = 's3bucket';
        prefix = 'prefix/with/object_depending_on_' + f'{today_date}' + 'T' + f'{today_time}'

        object = s3.list_objects_v2(Bucket=bucket,Prefix=prefix)
        # this try statement checks if there is a KeyError. assuming there is not it still confirms that the
        # key/file exists. Assuming it does return code of 200. Else the slack channel is notified.
        try:
            today_key = prefix 
            contents = object['Contents']
            for content in contents:
                key = content['Key']
                if today_key in key:
                    print('found: ' + key)
                    return {
                        'statusCode': 200,
                        'body': 'found file' + key
                    }
                else:
                    print('missing: ' + today_key)
                    slack.post(text='s3 File not found with time stamp ' + f'{today_key}')
                    return {
                        'statusCode': 204,
                        'body': 'File not found notify slack channel'
                    }
        # if a KeyError is returned then it also assumes that the key/file is missing and sends a notification to
        # the slack channel
        except KeyError:
            print('missing: ' + today_key)
            slack.post(text='s3 File not found with time stamp ' + f'{today_key}')
            return {
                'statusCode': 204,
                'body': 'Unexpected error or missing key in search'
            }
    #ENDOF else

# This is the daylight savings time check function. It also assumes this lambda func will be run at both times
# to account for either DST on or off. With this in mind it also checks in the nested if statement if the
# UTC time is even for dst or odd for no dst. Respectfully it will either execute the main lambda function
# if true or end if false.
def check_dst():
    if local_time[8] == 1:
        if (int(today_time) % 2) == 0:
            print('this is the time to execute code in dst')
            return True
        else:
            print('not the time to execute in dst')
            return False
    else:
        if (int(today_time) % 2) != 0:
            print('this is the time to execute code when not in dst')
            return True
        else:
            print('not the time to execute when not in dst')
            return False
